@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>編輯使用者</h1>
        @if (session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif

        <form method="POST" action="{{ route('user.update', ['user' => $user]) }}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">會員編號</label>
                <input type="text" name="member_number" id="member_number" class="form-control" value="{{ $user->ulid }}" readonly>
            </div>
            <div class="form-group">
                <label for="name">姓名</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}">
            </div>

            <button type="submit" class="btn btn-primary">更新使用者</button>
        </form>
    </div>
@endsection
