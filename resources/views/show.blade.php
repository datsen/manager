@extends('layouts.app')

@section('content')
<div class="container">
    <h1>使用者列表</h1>

    @if (session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
    @endif
        <ul>
            @foreach ($users as $user)
                <li>
                    <p><strong>會員編號:</strong> {{ $user->ulid }}</p>
                    <p><strong>姓名:</strong> {{ $user->name }}</p>
                    <p><strong>Email:</strong> {{ $user->email }}</p>
                    <a href="{{ route('user.edit', ['ulid' => $user->ulid]) }}" class="btn btn-primary">查看詳情</a>
                </li>
            @endforeach
        </ul>
</div>
@endsection
