<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['admin'])->group(function () {
    Route::get('/users', 'App\Http\Controllers\UserController@show')->name('user.show');
    Route::get('/user/{ulid}/edit', 'App\Http\Controllers\UserController@edit')->name('user.edit');
    Route::put('/user/{user}', 'App\Http\Controllers\UserController@update')->name('user.update');
});

