<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function show() {
        $user = $this->userService->getAll();
        return view('show', ['users' => $user]);
    }

    public function edit(string $ulid)
    {
        $user = $this->userService->edit($ulid);

        return view('edit', ['user' => $user]);
    }

    public function update(Request $request)
    {
        $memberNumber = $request->input('member_number');

        $validate = [
            "member_number" => 'required|string',
            "name" => "required|string",
            Rule::unique('Users', 'email')->ignore($memberNumber, 'ulid'),
        ];

        $data = $request->validate($validate);

        // 確認是否有使用者

        $user = $this->userService->edit($memberNumber);

        if($user) {
            $updatedUser = $this->userService->update($memberNumber, $data);

            if ($updatedUser) {
                return redirect()->route('user.show')->with('success', '使用者資訊已更新');
            }
        }
        return redirect()->route('user.edit', ['ulid' => $memberNumber])->with('error', '無法找到要更新的使用者');
    }

}
