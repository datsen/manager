<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    /**
     * getAll
     *
     * @return User[]
     */
    public function getAll(): ?array
    {
        return User::all()->all();

    }

    /**
     * getUserByUlid
     *
     * @param  string $ulid
     * @return User|null
     */
    public function getUserByUlid(string $ulid): ?User
    {
        $user = User::where('ulid', $ulid)->first();

        return $user;

    }
    /**
     * update
     *
     * @param  array $data
     * @return User|null
     */
    public function update(string $ulid, array $data): ?User
    {
        $user = $this->getUserByUlid($ulid);

        if($user) {
            $user->update($data);

            return $user;
        }

        return null;
    }
}
