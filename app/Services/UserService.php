<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * getAll
     *
     *  @return User[]
     */
    public function getAll(): array
    {
        return $this->userRepository->getAll();
    }

    /**
     * edit
     *
     * @param  string $ulid
     * @return User| null
     */
    public function edit(string $ulid): ?User
    {
        $user = $this->userRepository->getUserByUlid($ulid);

        if($user) {
            return $user;
        }
    }

    /**
     * update
     *
     * @param  array $data
     * @return User|null
     */
    public function update(string $ulid, array $data): ?User
    {
        $user = $this->userRepository->getUserByUlid($ulid);

        if($user) {
            $user->update($data);

            return $user;
        }

        return null;
    }

}
