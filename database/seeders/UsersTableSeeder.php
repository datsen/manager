<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        for ($i = 0; $i < 10; $i++) {
            $ulid = Str::uuid()->toString();
            $email = $this->generateUniqueEmail();
            $role = $this->generateRole();
            DB::table('users')->insert([
                'ulid' => $ulid,
                'name' => Str::random(10),
                'email' => $email,
                'role' => $role,
                'password' => bcrypt('secret'),
            ]);
        }
    }

    /**
     *  helper
     *  建立不重複email
     */
    private function generateUniqueEmail()
    {
        $email = Str::random(10) . '@gmail.com';

        // 检查生成的电子邮件是否已存在于数据库中
        while (User::where('email', $email)->exists()) {
            $email = Str::random(10) . '@gmail.com';
        }

        return $email;
    }

    /**
     * 建立角色
     */
    private function generateRole()
    {
        $roles = ['admin', 'usual'];
        $randomIndex = array_rand($roles, 1);
        $role = $roles[$randomIndex];

        return $role;
    }
}
